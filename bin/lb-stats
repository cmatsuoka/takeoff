#!/usr/bin/env ruby
# frozen_string_literal: true

# Example usage: lb -e production -r web -x <download/stats/drain/maint/ready>
#
# frozen_string_literal: true

require 'json'
require 'optparse'
require './config/roles'
require './config/takeoff'

DEFAULT_ENV = 'gstg'
ENV['TERM'] = 'xterm-256color'

class Parser
  def self.parse(options)
    args = {}

    opt_parser = OptionParser.new do |opts|
      opts.banner = 'Usage: version [options]'

      opts.on('-e', "--env=#{DEFAULT_ENV}", "Environment to use. Defaults to #{DEFAULT_ENV}.") do |env|
        args[:env] = env
      end

      opts.on('-h', '--help', 'Prints this help') do
        puts opts

        exit
      end
    end

    opt_parser.parse!(options)
    args
  end
end

options = Parser.parse(ARGV)

env = options[:env] || DEFAULT_ENV

require './lib/command_runner'
require './lib/progress_runner'
require './lib/lb/config'
require './lib/lb/command_wrapper'
require './lib/lb/map'
require 'pry'
require 'dispel'

wrapper = Lb::CommandWrapper.new(env)

def stats(wrapper)
  lb_map = Lb::Map.new

  stats = wrapper.stats

  stats.each_line do |line|
    items = line.gsub('UP UP', 'UP').strip.split.last.split(',')

    lb_map << items.values_at(0, 2)
  end

  lb_map
end

Dispel::Screen.open(colors: true) do |screen|
  puts "Loading..."
  screen.draw *stats(wrapper).show

  Dispel::Keyboard.output timeout: 2 do |key|
    if key == :timeout
      screen.draw *stats(wrapper).show
    elsif key == :"Ctrl+q" || key == "Ctrl+c"
      break
    elsif key == :"Ctrl+p"
      roles = Roles.new(env)
      regular_roles = roles.regular_roles

      puts "\n" * 30

      CommandRunner.run_command_on_roles(regular_roles,
                                         wrapper.ready,
                                         title: "Running ready in all nodes")
    end
  end
end

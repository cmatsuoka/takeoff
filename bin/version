#!/usr/bin/env ruby
# frozen_string_literal: true

# Example usage: version -e production -r web
#
# frozen_string_literal: true

require 'optparse'
require './config/roles'

DEFAULT_ENV = 'gprd'

class Parser
  def self.parse(options)
    args = {}

    opt_parser = OptionParser.new do |opts|
      opts.banner = 'Usage: version [options]'

      opts.on('-e', "--env=#{DEFAULT_ENV}", "Environment to deploy to. i.e gstg. Defaults to #{DEFAULT_ENV}.") do |env|
        args[:env] = env
      end

      opts.on('-r', '--role=api', 'Specify the role. Defaults to all.') do |role|
        args[:role] = role
      end

      opts.on('-h', '--help', 'Prints this help') do
        puts opts

        exit
      end
    end

    opt_parser.parse!(options)
    args
  end
end

options = Parser.parse(ARGV)

env = options[:env] || DEFAULT_ENV

require './lib/steps/version_check'
require './lib/steps/version_check_util'
require './lib/command_runner'
require './lib/progress_runner'

roles = Roles.new(env)
regular_roles = roles.regular_roles
role = regular_roles.find { |r| r.end_with?(options[:role]) } if options[:role]

Steps::VersionCheck.new(roles, role: role).run

#!/usr/bin/env ruby
# frozen_string_literal: true

require 'optparse'
require 'dotenv/load'
require './config/takeoff'
require 'semantic_logger'
require './lib/pid'

include SemanticLogger::Loggable

# We currently don't have canary in GCP.
WARM_UP_ENVIRONMENTS = %w[gprd gstg].freeze

class Parser
  def self.parse(options)
    args = {}

    opt_parser = OptionParser.new do |opts|
      opts.banner = 'Usage: takeoff-deploy [options]'

      opts.on('-e', '--env=gprd', 'Environment to deploy to. i.e gstg') do |env|
        args[:env] = env
      end

      opts.on('-s', '--skip-steps=pull_repo,load_balancing_check...', Array, 'Skip the steps listed (using name in steps.yml)') do |steps|
        args[:skip_steps] = steps
      end

      opts.on('-S', '--run-step=pull_repo,load_balancing_check...', Array, 'Run only the steps listed (using name in steps.yml)') do |steps|
        args[:run_steps] = steps
      end
      
      opts.on('-r', '--repo=REPO', "Specify the repo. Defaults to #{Takeoff.config[:default_repo]}") do |repo|
        args[:repo] = repo
      end

      opts.on('-v', '--version=VERSION', 'GitLab version i.e 10.1.0-rc1.ee.0') do |v|
        args[:version] = v
      end

      opts.on('-w', '--warm-up', 'Downloads packages instead of installing them (not compatible with -e)') do |w|
        args[:warmup] = w
      end

      opts.on('--[no-]stop-sidekiq', "HUP and stop Sidekiq during deploy. Defaults to #{Takeoff.config[:default_stop_sidekiq]}") do |stop|
        args[:stop_sidekiq] = stop
      end

      opts.on('--resume', 'Resume from the last step that has run') do |resume|
        args[:resume] = resume
      end

      opts.on('--use-ip', 'Use IP addresses instead of FQDNs') do
        Takeoff.config[:ip_address] = true
      end

      opts.on('--auto-resume', 'Keep resuming indefinitely [Experimental]') do |auto_resume|
        args[:resume] = auto_resume
        args[:auto_resume] = auto_resume
      end

      opts.on('--step-list', 'Shows the list of steps and its number') do
        Takeoff.steps.each_with_object([]).with_index do |(step, _), step_number|
          puts "Step #{step_number}. #{step.keys.first}"
        end

        exit
      end

      opts.on('-h', '--help', 'Prints this help') do
        puts opts

        exit
      end

      opts.on('--display-logs', 'Displays the logs after running the deploy') do
        args[:display_logs] = true
      end
    end

    opt_parser.parse!(options)
    args
  end
end

options = Parser.parse(ARGV)

# Load the rest of the libs after `Takeoff.config` is set
%w[./lib/*.rb ./lib/steps/*.rb ./lib/lb/*.rb ./config/*.rb].each do |dir|
  Dir[dir].each { |file| require file }
end

raise OptionParser::MissingArgument, 'version' unless options[:version]

unless ENV['TAKEOFF_SLACK_TOKEN'] || File.exist?('.env')
  raise ScriptError, 'Missing .env file or TAKEOFF_SLACK_TOKEN variable - see https://gitlab.com/gitlab-org/takeoff#set-up-your-own-takeoffenv'
end

options[:repo] ||= Takeoff.config[:default_repo]
options[:stop_sidekiq] ||= Takeoff.config[:default_stop_sidekiq]

# Setup logging
SemanticLogger.default_level = Takeoff.config[:log_level]
SemanticLogger.application = 'takeoff'

log_filename = ENV['RACK_ENV'] == 'test' ? 'test.log' : "#{options[:env] || 'warmup'}-#{options[:version]}"
regular_log_filename = "log/#{log_filename}.log"

SemanticLogger.add_appender(file_name: "log/#{log_filename}.json.log", formatter: :json)
SemanticLogger.add_appender(file_name: regular_log_filename, formatter: :color)
SemanticLogger.add_appender(file_name: "log/#{log_filename}.info.log", formatter: :color, level: :info)

Thread.current.name = 'main'

shared_status = SharedStatus.new(Time.now.utc)

begin
  if options[:warmup] || ENV['TAKEOFF_WARMUP']
    SlackWebhook.new(nil, options[:version], shared_status).warm_up

    logger.info('Warming up environments...')

    WARM_UP_ENVIRONMENTS.each do |env|
      puts "Warming up #{env} environment..."

      logger.measure_info('Warming up environment', payload: { environment: env, version: options[:version] }) do
        Steps::PackageWarmUp.new(
          version: options[:version],
          environment: env,
          repo: options[:repo],
          post_checks: ['retry_on_failure']
        ).run!
      end
    end
  else
    raise OptionParser::MissingArgument, 'env' unless options[:env]

    puts "Deploying v#{options[:version]} to #{options[:env]}"
    logger.info("Deploying v#{options[:version]} to #{options[:env]}", username: shared_status.user)

    logger.measure_info('GitLab deployment finished', payload: { environment: options[:env], version: options[:version], metric: 'deploy' }) do
      Pid.create
      StepRunner.new(options).run
    end
  end
ensure
  if options[:display_logs]
    SemanticLogger.flush

    puts("Log output:\n\n#{File.read(regular_log_filename)}")
  end
end

# Takeoff

**THIS REPOSITORY IS NOW DEPRECATED**

Note that this tool is now deprecated in favor of the new deployment tooling for
GitLab.com. For more information see:

* [release documentation for deployer](https://gitlab.com/gitlab-org/release/docs/blob/master/general/deploy/gitlab-com-deployer.md)
* [post-deployment patch documentation](https://gitlab.com/gitlab-org/release/docs/blob/master/general/deploy/post-deployment-patches.md)


Takeoff is the official GitLab deployment tool used for GitLab.com deployments.

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Contributing](#contributing)
- [Getting started](#getting-started)
  - [Install dependencies](#install-dependencies)
  - [Set up your own takeoff/.chef](#set-up-your-own-takeoffchef)
  - [Set up your own takeoff/.env](#set-up-your-own-takeoffenv)
- [Deploying GitLab](#deploying-gitlab)
  - [Overview](#overview)
  - [Requirements](#requirements)
  - [Security Releases](#security-releases)
  - [Regular GitLab.com Releases](#regular-gitlabcom-releases)
    - [Find The Version](#find-the-version)
    - [Determine If Downtime Is Necessary](#determine-if-downtime-is-necessary)
    - [Announce The Deployment](#announce-the-deployment)
    - [Disable GitLab For Downtime Deployments](#disable-gitlab-for-downtime-deployments)
    - [Running The Deploy](#running-the-deploy)
    - [Announce That The Deploy Is Done](#announce-that-the-deploy-is-done)
    - [Rolling Back Deployments](#rolling-back-deployments)
  - [Nightly dev.gitlab.org Releases](#nightly-devgitlaborg-releases)
    - [Building A Package](#building-a-package)
    - [Announce The Deploy](#announce-the-deploy)
    - [Deploy The Package](#deploy-the-package)
    - [Check The Deploy](#check-the-deploy)
  - [Troubleshooting & Tips](#troubleshooting--tips)
    - [Pinging Hosts](#pinging-hosts)
    - [Stopping Mailroom](#stopping-mailroom)
    - [Checking Package Versions](#checking-package-versions)
    - [Checking Pending Migrations](#checking-pending-migrations)
    - [Executing Pending Migrations](#executing-pending-migrations)
    - [Upgrading Pets](#upgrading-pets)
- [License](#license)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Contributing

Takeoff is an open source project and we are very happy to accept community contributions. Please refer to [CONTRIBUTING.md](/CONTRIBUTING.md) for details.

## Getting started

### Install dependencies

```
$ bundle install
```

### Set up your own takeoff/.chef

- Ensure you have a `.chef` folder in your takeoff repo directory. If one does not exist create one.
- Use the `knife.rb.example` file to create a `.chef/knife.rb` file:

```bash
$ cp knife.rb.example .chef/knife.rb
```

- In order to continue you'll need a `pem` key. Someone from the production team will need to take care of this, so either create a [new infrastructure permissions issue](https://gitlab.com/gitlab-com/infrastructure/issues/new?issue%5Btitle%5D=Chef%20and%20SSH%20access%20request%20for%20YOUR%20NAME) to request Chef acces, or ask someone in #production channel.
- If you recieved the `pem` key directly. You can skip the next 3 steps and add/move your `pem` key to the `.chef` directory in your takeoff repo as `username.pem` (replace 'username' with your username).

- When someone from production team creates your `.pem` key in `deploy.gitlab.com`, we need to copy it to the `takeoff` repo, so first thing open your terminal and type:

```bash
# Replace 'username' with your username

$ ssh username@deploy.gitlab.com
```

Verify that there's a `username.pem` file in the home dir:

```bash
$ ls -la
```

Open a new terminal window, go to your `takeoff` repo and copy the `.pem` file from `deploy.gitlab.com` into your `.chef` folder:

```bash
$ rsync -avhS username@deploy.gitlab.com:~/username.pem ./.chef
```

Open `.chef/knife.rb` file and update the template username `janedoe` to your username. The following 2 lines will need to be updated:
```
node_name                "janedoe"
client_key               "#{current_dir}/janedoe.pem"
```

If your local username is different that the remote ssh username make sure to update the line below.
Uncomment the line and replace `janedoe` with your ssh username.

```
#knife[:ssh_user] = 'janedoe'
```

Make sure everything is working by running (you should see a list of servers):

```bash
$ knife status
```

Now that everything is working, we need to remove our `.pem` file from `deploy.gitlab.com`, go back to the terminal window where you ssh into `deploy.gitlab.com` and type:

```bash
$ rm username.pem
$ exit
```

### Set up your own takeoff/.env

Copy the `.env.example` file to `.env`

Replace the `TAKEOFF_SLACK_TOKEN` value with the correct one from `1password`.

## Deploying GitLab

### Overview

This document describes the procedures for deploying new versions of GitLab to
all GitLab.com environments.

### Requirements

Before one can deploy there are a few requirements that have to be met, these
are as follows:

* Commit access to <https://dev.gitlab.org/gitlab/omnibus-gitlab/>
* You need to have access to our Chef server, in particular you need to be able
  to run `knife` commands.
* You will need SSH access to the servers to deploy to.
* All dependencies of the takeoff repository need to be installed, this can be
  taken care of by running `bundle install` in the repository.
* You need to be part of the `release-manager` group in Cog. Ask one of the
  Cog admins in `#production` to run `!group-member-add release-manager <your handle>`.
* Make sure you create a working `knife.rb` and chef key in `.chef`.

The rest of this guide assumes all of these requirements have been met.

### Security Releases

Sometimes you might need to deploy a release from a private repository. For
example, releases with critical security fixes are typically deployed from a
private repository. This allows us to update GitLab.com before announcing the
vulnerability to the public.

For more information on this process see ["Deploying from a private
repository"](doc/private-repository-deploy.md).

### Regular GitLab.com Releases

A regular release is done using packages from our public packagecloud
repository, located at <https://packages.gitlab.com/>.

The deployment procedures listed below are the same for both staging and
production.

**NOTE:** Be aware that production deploys **DO NOT** update the redis and postgres servers by default.
If relevant changes have to be made (e.g. new critical omnibus postgres version) look [here](#upgrading-pets).

#### Find The Version

The first step is to figure out what the version number to deploy is, and what
repository the package resides in. For GitLab.com (both for production and
staging) we deploy GitLab Enterprise Edition. To find the correct version,
follow these steps:

1. Go to <https://packages.gitlab.com/app/gitlab/gitlab-ee/search?filter=all&q=gitlab-ee&dist=ubuntu%2Fxenial>:
   this page shows all EE packages for ubuntu/xenial.
1. Find the package for the version you would like to deploy.
1. Click on the link to the package.
1. Take note of the version number found in the `sudo apt-get install` line.

For example, say we want to deploy GitLab EE 8.10.13. Following steps 1 to 3
we'd end up at <https://packages.gitlab.com/gitlab/gitlab-ee/packages/ubuntu/xenial/gitlab-ee_8.10.13-ee.0_amd64.deb>.
This page shows the following:

```
curl -s https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
sudo apt-get install gitlab-ee=8.10.13-ee.0
```

Here the version number we need is `8.10.13-ee.0` (including the `-ee.0`
suffix).

#### Determine If Downtime Is Necessary

Sometimes downtime is inevitable. Downtime might be necessary because of
database migrations, or because of some other kind of upgrade (e.g. a database
upgrade). In this case some extra steps are required. A release manager should
be aware of any downtime requirements and communicate this clearly. When in
doubt, ask!

GitLab provides a Rake task called `gitlab:db:downtime_check`. This task can be
used if migrations since a given release require downtime. For example:

```
bundle exec rake "gitlab:db:downtime_check[v8.16.0]"
```

This would check if any migrations since the tag v8.16.0 require downtime. This
requires that the current branch/tag you're on is up to date.

**NOTE:** Be aware that production deploys **DO NOT** update the redis and postgres servers
by default to ensure that these upgrades do not cause downtimes. If relavent changes
have to be made (e.g. new critical omnibus postgres version) look [here](#upgrading-pets).

#### Announce The Deployment

Refer to the [Announce a deployment](doc/announce-a-deployment.md) document.

#### Disable GitLab For Downtime Deployments

**NOTE:** this will take the GitLab.com cluster offline, **only** run this if
absolutely necessary.

If you have determined that downtime is necessary you need to take the cluster
offline.

We need to enable the deployment page:

```
bundle exec rake deploy_page:enable                        # for production
bundle exec rake deploy_page:enable[gitlab-staging-worker] # for staging
```

Enabling the deploy page will automatically make Unicorn unreachable.

#### Running The Deploy

Now we can run the actual deploy using a new command, takeoff-deploy:

For example, to warm up, and then deploy to staging:

```
bin/takeoff-deploy -v VERSION -w
bin/takeoff-deploy -e staging -v VERSION
```

The `-w` flag will warm up the installation of the packages by downloading them in advance.
It's required to run on all environments before proceeding to the actual deployment,
and ideally, right after the packages are available. Not doing so, may throw an error if the packages
are not available, as we didn't perform an `apt-get update`.

By default, we don't stop Sidekiq. Please use the `--stop-sidekiq` if there are migrations that
may cause potential issues, so sidekiq is stopped beforehand.

If something went wrong and the deploy stopped, we can resume at the last step by passing a
`--resume` flag.

Here VERSION should be replaced with the package version noted down earlier. See the full
list of options running:

```
bin/takeoff-deploy --help
```

**NOTE:** If the version includes "-rcX" with "X" being a number the task will
automatically use the gitlab/pre-release repository.

These commands will take care of everything needed to deploy, such as (but not
limited to):

* Updating Chef roles
* Running database migrations, and post-deployment migrations
* Tracking deployments
* Making sure all processes are running

#### Announce That The Deploy Is Done

Once deployed you should send a public announcement that the deploy has been
completed. This only has to be done via Twitter using the `!tweet` chat command
in the `#production` channel. Optionally you can also remove the broadcast
message using the GitLab UI, but this isn't required.

#### Rolling Back Deployments

Sometimes it might be necessary to roll back a deployment. In this case the
deployment procedure is exactly the same, except you use the previous version of
a package instead of the latest one.

### Nightly dev.gitlab.org Releases

Our private GitLab instance located at <https://dev.gitlab.org/> is deployed
every day using a nightly package from the [nightly-builds repository]. For
<https://dev.gitlab.org> we deploy **GitLab CE**.

The nightly package contains the revision of master branch available at the
moment of the package build. For example, the nightly package will be built from
revision of omnibus-gitlab master branch available on <https://dev.gitlab.org/>
and it will contain the revision of GitLab Rails master branch available at that
moment on <https://dev.gitlab.org/>.

To force a deployment you first need to make sure that the various GitLab
repositories on <https://dev.gitlab.org/> are up to date. This includes
repository such as gitlab-ee, omnibus-gitlab, gitlab-shell, etc.

#### Building A Package

Go to [omnibus-gitlab triggers page] and copy the "Nightly" token. Then, run the
following command while replacing TOKEN with the obtained token:

```
curl --silent -X POST -F token=TOKEN -F 'variables[NIGHTLY=true]' https://dev.gitlab.org/api/v3/projects/283/trigger/builds
```

#### Announce The Deploy

Since dev.gitlab.org is our private instance you do not need to announce a
deploy via Twitter. A deploy notification will automatically be posted to #announcements.

#### Deploy The Package

Check the [omnibus-gitlab builds] page or [nightly-builds repository] for the
new Ubuntu 16.04 package. Once you've confirmed that the new packages are
available, run:

```
bundle exec knife ssh 'name:dev.gitlab.org' 'sudo apt-get update && sudo apt-get install gitlab-ce'
```

This should automatically download and install the new package and run a
package reconfigure.

#### Check The Deploy

SSH into dev.gitlab.org and run the following:

```
sudo gitlab-ctl status
sudo gitlab-rake db:migrate:status
```

### Troubleshooting & Tips

Sometimes things don't go entirely as planned. For example, a deploy might fail
because the JSON of a role has not been changed (e.g. this might happen when
running the same task twice). Fear not, for most of the steps can be performed
individually.

#### Pinging Hosts

There is a PING utility available that can be used to find out which hosts are not
responding on production.

In order to ping all production hosts you can call it by running:

```
$ bin/ping
```

You can also ping a specific set of hosts. For example:

```
$ bin/ping web
Pinging web nodes...
web-09.sv.prd.gitlab.com is down!
```

#### Stopping Mailroom

Sometimes you may need to manually stop mailroom. This can be done by running:

```
bundle exec rake mailroom:stop
```

To check if Mailroom is running:

```
bundle exec rake check:mailroom
```

#### Checking Package Versions

To check if all hosts are running the same GitLab version, run the following:

```
bin/version
```

If all hosts are running the same version this will show something along the
lines of:

```
✓ GitLab version for gitlab-cluster-worker (7.21 sec)
New version: 8.16.3-ee
```

To check a specific environment:

```
bin/version -e staging
```

To check a specific environment and role:

```
bin/version -e staging -r web
```

#### Checking Pending Migrations

To check for any pending migrations, run the following:

```
bundle exec rake check:migrations
```

#### Executing Pending Migrations

To execute any pending migrations, simply run:

```
bundle exec knife ssh -a ipaddress 'role:gitlab-cluster-worker-blessed' 'sudo gitlab-rake db:migrate'
```

This will execute the migrations on the blessed worker.

[GitLab CE repository from GitLab.com]: https://gitlab.com/gitlab-org/gitlab-ce
[GitLab CE repository on dev.gitlab.org]: https://dev.gitlab.org/gitlab/gitlabhq
[nightly-builds repository]: https://packages.gitlab.com/gitlab/nightly-builds
[omnibus-gitlab triggers page]: https://dev.gitlab.org/gitlab/omnibus-gitlab/triggers
[omnibus-gitlab builds]: https://dev.gitlab.org/gitlab/omnibus-gitlab/builds

#### Upgrading Pets
In production we have socalled pets (servers whose downtime can effect all of GitLab.com).
These include the database and redis instances. Inorder to mitigate upgrade related downtime
we have opted to pin the version of the omnibus package directly on the
[db](https://dev.gitlab.org/cookbooks/chef-repo/blob/master/roles/gitlab-cluster-db.json)
and [redis](https://dev.gitlab.org/cookbooks/chef-repo/blob/master/roles/gitlab-cluster-redis.json)
chef roles via [override_attributes](https://docs.chef.io/attributes.html#attribute-types).

e.g.
```ruby
"override_attributes": {
  "omnibus-gitlab": {
    "package":{
      "version": "9.1.2-ee.0"
    }
  }
}
```
This allows us to control when these pets are upgraded, and do so in a controlled, no downtime
manner. This should be coordinated with the production team by creating a ticket
[here](https://gitlab.com/gitlab-com/infrastructure). They will then upgrade the roles and servers
in a safe way.

## License

Takeoff is distributed under the MIT license, see the [LICENSE](/LICENSE) for details.

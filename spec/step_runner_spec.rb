# frozen_string_literal: true

require 'spec_helper'
require './lib/step_runner'
require './lib/lb/command_wrapper'
require './lib/lb/config'

describe StepRunner do
  let(:version_output) do
    <<~OUTPUT
    web-01-sv-gstg.c.gitlab-staging-1.internal                Current Version: gitlab-ee 10.0.2-ee.0 web-01-sv-gstg.c.gitlab-staging-1.internal
    api-01-sv-gstg.c.gitlab-staging-1.internal                Current Version: gitlab-ee 10.0.2-ee.0 api-01-sv-gstg.c.gitlab-staging-1.internal
    sidekiq-pages-01-sv-gstg.c.gitlab-staging-1.internal      Current Version: gitlab-ee 10.0.2-ee.0 sidekiq-pages-01-sv-gstg.c.gitlab-staging-1.internal
    sidekiq-pullmirror-01-sv-gstg.c.gitlab-staging-1.internal Current Version: gitlab-ee 10.0.2-ee.0 sidekiq-pullmirror-01-sv-gstg.c.gitlab-staging-1.internal
    git-01-sv-gstg.c.gitlab-staging-1.internal                Current Version: gitlab-ee 10.0.2-ee.0 git-01-sv-gstg.c.gitlab-staging-1.internal
    file-01-stor-gstg.c.gitlab-staging-1.internal             Current Version: gitlab-ee 10.0.2-ee.0 file-01-stor-gstg.c.gitlab-staging-1.internal
    sidekiq-besteffort-01-sv-gstg.c.gitlab-staging-1.internal Current Version: gitlab-ee 10.0.2-ee.0 sidekiq-besteffort-01-sv-gstg.c.gitlab-staging-1.internal
    mailroom-01-sv-gstg.c.gitlab-staging-1.internal           Current Version: gitlab-ee 10.0.2-ee.0 mailroom-01-sv-gstg.c.gitlab-staging-1.internal
    registry-01-sv-gstg.c.gitlab-staging-1.internal           Current Version: gitlab-ee 10.0.2-ee.0 registry-01-sv-gstg.c.gitlab-staging-1.internal
    sidekiq-realtime-01-sv-gstg.c.gitlab-staging-1.internal   Current Version: gitlab-ee 10.0.2-ee.0 sidekiq-realtime-01-sv-gstg.c.gitlab-staging-1.internal
    sidekiq-besteffort-02-sv-gstg.c.gitlab-staging-1.internal Current Version: gitlab-ee 10.0.2-ee.0 sidekiq-besteffort-02-sv-gstg.c.gitlab-staging-1.internal
    file-02-stor-gstg.c.gitlab-staging-1.internal             Current Version: gitlab-ee 10.0.2-ee.0 file-02-stor-gstg.c.gitlab-staging-1.internal
    sidekiq-traces-01-sv-gstg.c.gitlab-staging-1.internal     Current Version: gitlab-ee 10.0.2-ee.0 sidekiq-traces-01-sv-gstg.c.gitlab-staging-1.internal
    sidekiq-asap-01-sv-gstg.c.gitlab-staging-1.internal       Current Version: gitlab-ee 10.0.2-ee.0 sidekiq-asap-01-sv-gstg.c.gitlab-staging-1.internal
    sidekiq-besteffort-03-sv-gstg.c.gitlab-staging-1.internal Current Version: gitlab-ee 10.0.2-ee.0 sidekiq-besteffort-03-sv-gstg.c.gitlab-staging-1.internal
    OUTPUT
  end
  let(:runner_output) do
    <<~OUTPUT
      git checkout master
      git pull git@gitlab.com:gitlab-org/takeoff.git master
      bundle exec knife download roles/gstg-base-deploy-node.json
      Ignoring load balancing check
      bundle exec knife ssh -e -a fqdn roles:gstg-base-stor-gitaly OR roles:gstg-base-be-sidekiq OR roles:gstg-base-be-mailroom OR roles:gstg-base-fe-web OR roles:gstg-base-fe-web-pages OR roles:gstg-base-fe-api OR roles:gstg-base-fe-git OR roles:gstg-base-fe-registry OR roles:gstg-base-deploy-node sudo service chef-client stop
      bundle exec knife download roles/gstg-omnibus-version.json
      Ignoring version update to gstg
      bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure
      bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo SKIP_POST_DEPLOYMENT_MIGRATIONS=1 gitlab-rake db:migrate
      bundle exec knife search 'roles:gstg-base-stor-gitaly' -a fqdn -Fjson
      bundle exec knife search 'roles:gstg-base-deploy-node' -a fqdn -Fjson
      bundle exec knife ssh -e -a fqdn 'fqdn:fqdn1' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITALY_SERVER_VERSION 2>/dev/null'
      bundle exec knife ssh -e -a fqdn 'fqdn:fqdn2' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITALY_SERVER_VERSION 2>/dev/null'
      bundle exec knife ssh -e -a fqdn roles:gstg-base-stor-gitaly sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure
      bundle exec knife ssh -e -a fqdn roles:gstg-base-stor-gitaly sudo gitlab-ctl restart gitaly
      bundle exec knife search 'roles:gstg-base-fe-registry' -a fqdn -Fjson
      bundle exec knife search 'roles:gstg-base-deploy-node' -a fqdn -Fjson
      bundle exec knife ssh -e -a fqdn 'fqdn:fqdn1' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_PAGES_VERSION 2>/dev/null'
      bundle exec knife ssh -e -a fqdn 'fqdn:fqdn2' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_PAGES_VERSION 2>/dev/null'
      bundle exec knife search 'roles:gstg-base-fe-registry' -a fqdn -Fjson
      bundle exec knife search 'roles:gstg-base-deploy-node' -a fqdn -Fjson
      bundle exec knife ssh -e -a fqdn 'fqdn:fqdn1' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_WORKHORSE_VERSION 2>/dev/null'
      bundle exec knife ssh -e -a fqdn 'fqdn:fqdn2' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_WORKHORSE_VERSION 2>/dev/null'
      bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn
      bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo gitlab-ctl restart sidekiq-cluster
      bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo gitlab-ctl restart nginx
      bundle exec knife ssh -e -a fqdn roles:gstg-base-be-mailroom sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure
      bundle exec knife ssh -e -a fqdn roles:gstg-base-be-mailroom sudo gitlab-ctl restart
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn && sleep 3"
      bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl restart deploy-page
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart gitlab-workhorse && sleep 3"
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart nginx && sleep 3"
      bundle exec knife ssh -e -C 1 -a fqdn roles:gstg-base-fe-web-pages sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sleep 3"
      bundle exec knife ssh -e -C 1 -a fqdn roles:gstg-base-fe-web-pages sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart gitlab-pages && sleep 3"
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-api sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn && sleep 3"
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-api sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart gitlab-workhorse && sleep 3"
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-api sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart nginx && sleep 3"
      bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-api sudo gitlab-ctl restart registry
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-git sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn && sleep 3"
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-git sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart gitlab-workhorse && sleep 3"
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-git sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart nginx && sleep 3"
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-registry sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sleep 3"
      bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-registry sudo gitlab-ctl restart
      bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo gitlab-rake gitlab:track_deployment
      bundle exec knife ssh -e -a fqdn roles:gstg-base-stor-gitaly OR roles:gstg-base-be-sidekiq OR roles:gstg-base-be-mailroom OR roles:gstg-base-fe-web OR roles:gstg-base-fe-web-pages OR roles:gstg-base-fe-api OR roles:gstg-base-fe-git OR roles:gstg-base-fe-registry OR roles:gstg-base-deploy-node sudo -E service chef-client start
      bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo gitlab-rake db:migrate
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-api OR roles:gstg-base-fe-git OR roles:gstg-base-be-sidekiq OR roles:gstg-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl hup unicorn && sleep 3"
      bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo /opt/gitlab/init/sidekiq 1 || sudo /opt/gitlab/init/sidekiq-cluster 1
      bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo gitlab-ctl restart sidekiq-cluster
      bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl start
      bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl deploy-page down
      bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo gitlab-rake db:migrate:status
      New version: 10.0.2-ee.0
    OUTPUT
  end

  before do
    enable_dry_run

    allow_any_instance_of(Steps::VersionCheck).to receive(:dpkg_gitlab_version).and_return(version_output)
    allow_any_instance_of(Resumer).to receive(:save)
    allow_any_instance_of(Lb::Config).to receive(:lb_list).and_return('lb-1:lb-2')

    # Avoid Threads here so the output is not random
    allow_any_instance_of(Steps::DeployMediator).to receive(:run_steps!) do |mediator|
      mediator.send(:concurrent_steps).map { |step| step.run! }
    end
  end

  after do
    STDOUT.flush

    enable_dry_run
  end

  subject { described_class.new(version: '10.0.2-ee.0', env: 'gstg', repo: 'gitlab/test', stop_sidekiq: Takeoff.config[:default_stop_sidekiq]) }

  it 'outputs the right command' do
    expect(subject).not_to receive(:abort)

    expect { subject.run }.to output(runner_output).to_stdout_from_any_process
  end

  context 'error starting chef' do
    let(:retry_runner_output) do
      <<~OUTPUT
        git checkout master
        git pull git@gitlab.com:gitlab-org/takeoff.git master
        bundle exec knife download roles/gstg-base-deploy-node.json
        Ignoring load balancing check
        Retrying Steps::ChefStop - Attempt 1 - Error: test error
        bundle exec knife download roles/gstg-omnibus-version.json
        Ignoring version update to gstg
        bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure
        bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo SKIP_POST_DEPLOYMENT_MIGRATIONS=1 gitlab-rake db:migrate
        bundle exec knife search 'roles:gstg-base-stor-gitaly' -a fqdn -Fjson
        bundle exec knife search 'roles:gstg-base-deploy-node' -a fqdn -Fjson
        bundle exec knife ssh -e -a fqdn 'fqdn:fqdn1' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITALY_SERVER_VERSION 2>/dev/null'
        bundle exec knife ssh -e -a fqdn 'fqdn:fqdn2' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITALY_SERVER_VERSION 2>/dev/null'
        bundle exec knife ssh -e -a fqdn roles:gstg-base-stor-gitaly sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure
        bundle exec knife ssh -e -a fqdn roles:gstg-base-stor-gitaly sudo gitlab-ctl restart gitaly
        bundle exec knife search 'roles:gstg-base-fe-registry' -a fqdn -Fjson
        bundle exec knife search 'roles:gstg-base-deploy-node' -a fqdn -Fjson
        bundle exec knife ssh -e -a fqdn 'fqdn:fqdn1' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_PAGES_VERSION 2>/dev/null'
        bundle exec knife ssh -e -a fqdn 'fqdn:fqdn2' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_PAGES_VERSION 2>/dev/null'
        bundle exec knife search 'roles:gstg-base-fe-registry' -a fqdn -Fjson
        bundle exec knife search 'roles:gstg-base-deploy-node' -a fqdn -Fjson
        bundle exec knife ssh -e -a fqdn 'fqdn:fqdn1' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_WORKHORSE_VERSION 2>/dev/null'
        bundle exec knife ssh -e -a fqdn 'fqdn:fqdn2' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_WORKHORSE_VERSION 2>/dev/null'
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo gitlab-ctl restart sidekiq-cluster
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo gitlab-ctl restart nginx
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-mailroom sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-mailroom sudo gitlab-ctl restart
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn && sleep 3"
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl restart deploy-page
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart gitlab-workhorse && sleep 3"
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart nginx && sleep 3"
        bundle exec knife ssh -e -C 1 -a fqdn roles:gstg-base-fe-web-pages sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sleep 3"
        bundle exec knife ssh -e -C 1 -a fqdn roles:gstg-base-fe-web-pages sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart gitlab-pages && sleep 3"
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-api sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn && sleep 3"
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-api sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart gitlab-workhorse && sleep 3"
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-api sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart nginx && sleep 3"
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-api sudo gitlab-ctl restart registry
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-git sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn && sleep 3"
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-git sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart gitlab-workhorse && sleep 3"
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-git sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart nginx && sleep 3"
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-registry sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sleep 3"
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-registry sudo gitlab-ctl restart
        bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo gitlab-rake gitlab:track_deployment
        bundle exec knife ssh -e -a fqdn roles:gstg-base-stor-gitaly OR roles:gstg-base-be-sidekiq OR roles:gstg-base-be-mailroom OR roles:gstg-base-fe-web OR roles:gstg-base-fe-web-pages OR roles:gstg-base-fe-api OR roles:gstg-base-fe-git OR roles:gstg-base-fe-registry OR roles:gstg-base-deploy-node sudo -E service chef-client start
        bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo gitlab-rake db:migrate
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-api OR roles:gstg-base-fe-git OR roles:gstg-base-be-sidekiq OR roles:gstg-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl hup unicorn && sleep 3"
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo /opt/gitlab/init/sidekiq 1 || sudo /opt/gitlab/init/sidekiq-cluster 1
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo gitlab-ctl restart sidekiq-cluster
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl start
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl deploy-page down
        bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo gitlab-rake db:migrate:status
        New version: 10.0.2-ee.0
      OUTPUT
    end

    it 'shows the error and retry attempt' do
      allow_any_instance_of(Steps::ChefStop).to receive(:roles_to_stop).and_raise('test error')

      expect_any_instance_of(Steps::ChefStop).to receive(:abort).once

      expect { subject.run }.to output(retry_runner_output).to_stdout_from_any_process
    end
  end

  context 'sidekiq enabled' do
    let(:runner_output) do
      <<~OUTPUT
      git checkout master
      git pull git@gitlab.com:gitlab-org/takeoff.git master
      bundle exec knife download roles/gstg-base-deploy-node.json
      Ignoring load balancing check
      bundle exec knife ssh -e -a fqdn roles:gstg-base-stor-gitaly OR roles:gstg-base-be-sidekiq OR roles:gstg-base-be-mailroom OR roles:gstg-base-fe-web OR roles:gstg-base-fe-web-pages OR roles:gstg-base-fe-api OR roles:gstg-base-fe-git OR roles:gstg-base-fe-registry OR roles:gstg-base-deploy-node sudo service chef-client stop
      bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo /opt/gitlab/init/sidekiq 1 || sudo /opt/gitlab/init/sidekiq-cluster 1
      bundle exec knife ssh -e -a fqdn roles:gstg-base-be-mailroom sudo gitlab-ctl stop mailroom
      bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo gitlab-ctl stop sidekiq-cluster
      bundle exec knife download roles/gstg-omnibus-version.json
      Ignoring version update to gstg
      bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure
      bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo SKIP_POST_DEPLOYMENT_MIGRATIONS=1 gitlab-rake db:migrate
      bundle exec knife search 'roles:gstg-base-stor-gitaly' -a fqdn -Fjson
      bundle exec knife search 'roles:gstg-base-deploy-node' -a fqdn -Fjson
      bundle exec knife ssh -e -a fqdn 'fqdn:fqdn1' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITALY_SERVER_VERSION 2>/dev/null'
      bundle exec knife ssh -e -a fqdn 'fqdn:fqdn2' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITALY_SERVER_VERSION 2>/dev/null'
      bundle exec knife ssh -e -a fqdn roles:gstg-base-stor-gitaly sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure
      bundle exec knife ssh -e -a fqdn roles:gstg-base-stor-gitaly sudo gitlab-ctl restart gitaly
      bundle exec knife search 'roles:gstg-base-fe-registry' -a fqdn -Fjson
      bundle exec knife search 'roles:gstg-base-deploy-node' -a fqdn -Fjson
      bundle exec knife ssh -e -a fqdn 'fqdn:fqdn1' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_PAGES_VERSION 2>/dev/null'
      bundle exec knife ssh -e -a fqdn 'fqdn:fqdn2' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_PAGES_VERSION 2>/dev/null'
      bundle exec knife search 'roles:gstg-base-fe-registry' -a fqdn -Fjson
      bundle exec knife search 'roles:gstg-base-deploy-node' -a fqdn -Fjson
      bundle exec knife ssh -e -a fqdn 'fqdn:fqdn1' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_WORKHORSE_VERSION 2>/dev/null'
      bundle exec knife ssh -e -a fqdn 'fqdn:fqdn2' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_WORKHORSE_VERSION 2>/dev/null'
      bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn
      bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo gitlab-ctl restart sidekiq-cluster
      bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo gitlab-ctl restart nginx
      bundle exec knife ssh -e -a fqdn roles:gstg-base-be-mailroom sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure
      bundle exec knife ssh -e -a fqdn roles:gstg-base-be-mailroom sudo gitlab-ctl restart
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn && sleep 3"
      bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl restart deploy-page
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart gitlab-workhorse && sleep 3"
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart nginx && sleep 3"
      bundle exec knife ssh -e -C 1 -a fqdn roles:gstg-base-fe-web-pages sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sleep 3"
      bundle exec knife ssh -e -C 1 -a fqdn roles:gstg-base-fe-web-pages sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart gitlab-pages && sleep 3"
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-api sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn && sleep 3"
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-api sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart gitlab-workhorse && sleep 3"
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-api sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart nginx && sleep 3"
      bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-api sudo gitlab-ctl restart registry
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-git sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn && sleep 3"
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-git sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart gitlab-workhorse && sleep 3"
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-git sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart nginx && sleep 3"
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-registry sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sleep 3"
      bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-registry sudo gitlab-ctl restart
      bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo gitlab-rake gitlab:track_deployment
      bundle exec knife ssh -e -a fqdn roles:gstg-base-stor-gitaly OR roles:gstg-base-be-sidekiq OR roles:gstg-base-be-mailroom OR roles:gstg-base-fe-web OR roles:gstg-base-fe-web-pages OR roles:gstg-base-fe-api OR roles:gstg-base-fe-git OR roles:gstg-base-fe-registry OR roles:gstg-base-deploy-node sudo -E service chef-client start
      bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo gitlab-rake db:migrate
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-api OR roles:gstg-base-fe-git OR roles:gstg-base-be-sidekiq OR roles:gstg-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl hup unicorn && sleep 3"
      bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo /opt/gitlab/init/sidekiq 1 || sudo /opt/gitlab/init/sidekiq-cluster 1
      bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo gitlab-ctl restart sidekiq-cluster
      bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl start
      bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl deploy-page down
      bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo gitlab-rake db:migrate:status
      New version: 10.0.2-ee.0
      OUTPUT
    end

    subject { described_class.new(version: '10.0.2-ee.0', env: 'gstg', repo: 'gitlab/test', stop_sidekiq: true) }

    it 'outputs the right command' do
      expect(subject).not_to receive(:abort)

      expect { subject.run }.to output(runner_output).to_stdout_from_any_process
    end
  end

  context 'gitaly, workhorse and pages with the same version' do
    let(:runner_output) do
      <<~OUTPUT
      git checkout master
      git pull git@gitlab.com:gitlab-org/takeoff.git master
      bundle exec knife download roles/gstg-base-deploy-node.json
      Ignoring load balancing check
      bundle exec knife ssh -e -a fqdn roles:gstg-base-stor-gitaly OR roles:gstg-base-be-sidekiq OR roles:gstg-base-be-mailroom OR roles:gstg-base-fe-web OR roles:gstg-base-fe-web-pages OR roles:gstg-base-fe-api OR roles:gstg-base-fe-git OR roles:gstg-base-fe-registry OR roles:gstg-base-deploy-node sudo service chef-client stop
      bundle exec knife download roles/gstg-omnibus-version.json
      Ignoring version update to gstg
      bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure
      bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo SKIP_POST_DEPLOYMENT_MIGRATIONS=1 gitlab-rake db:migrate
      bundle exec knife ssh -e -a fqdn roles:gstg-base-stor-gitaly sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure
      bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn
      bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo gitlab-ctl restart sidekiq-cluster
      bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo gitlab-ctl restart nginx
      bundle exec knife ssh -e -a fqdn roles:gstg-base-be-mailroom sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure
      bundle exec knife ssh -e -a fqdn roles:gstg-base-be-mailroom sudo gitlab-ctl restart
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn && sleep 3"
      bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl restart deploy-page
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart nginx && sleep 3"
      bundle exec knife ssh -e -C 1 -a fqdn roles:gstg-base-fe-web-pages sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sleep 3"
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-api sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn && sleep 3"
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-api sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart nginx && sleep 3"
      bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-api sudo gitlab-ctl restart registry
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-git sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn && sleep 3"
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-git sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart nginx && sleep 3"
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-registry sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sleep 3"
      bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-registry sudo gitlab-ctl restart
      bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo gitlab-rake gitlab:track_deployment
      bundle exec knife ssh -e -a fqdn roles:gstg-base-stor-gitaly OR roles:gstg-base-be-sidekiq OR roles:gstg-base-be-mailroom OR roles:gstg-base-fe-web OR roles:gstg-base-fe-web-pages OR roles:gstg-base-fe-api OR roles:gstg-base-fe-git OR roles:gstg-base-fe-registry OR roles:gstg-base-deploy-node sudo -E service chef-client start
      bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo gitlab-rake db:migrate
      bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-api OR roles:gstg-base-fe-git OR roles:gstg-base-be-sidekiq OR roles:gstg-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl hup unicorn && sleep 3"
      bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo /opt/gitlab/init/sidekiq 1 || sudo /opt/gitlab/init/sidekiq-cluster 1
      bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo gitlab-ctl restart sidekiq-cluster
      bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl start
      bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl deploy-page down
      bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo gitlab-rake db:migrate:status
      New version: 10.0.2-ee.0
      OUTPUT
    end

    subject { described_class.new(version: '10.0.2-ee.0', env: 'gstg', repo: 'gitlab/test', stop_sidekiq: false) }

    before do
      allow_any_instance_of(VersionChange).to receive(:changed?).and_return(false)
    end

    it 'outputs the right command' do
      expect(subject).not_to receive(:abort)

      expect { subject.run }.to output(runner_output).to_stdout_from_any_process
    end
  end

  context 'skip steps' do
    let(:runner_output) do
      <<~OUTPUT
      bundle exec knife ssh -e -a fqdn roles:gstg-base-stor-gitaly OR roles:gstg-base-be-sidekiq OR roles:gstg-base-be-mailroom OR roles:gstg-base-fe-web OR roles:gstg-base-fe-web-pages OR roles:gstg-base-fe-api OR roles:gstg-base-fe-git OR roles:gstg-base-fe-registry OR roles:gstg-base-deploy-node sudo service chef-client stop
      bundle exec knife search 'roles:gstg-base-stor-gitaly' -a fqdn -Fjson
      bundle exec knife search 'roles:gstg-base-deploy-node' -a fqdn -Fjson
      bundle exec knife ssh -e -a fqdn 'fqdn:fqdn1' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITALY_SERVER_VERSION 2>/dev/null'
      bundle exec knife ssh -e -a fqdn 'fqdn:fqdn2' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITALY_SERVER_VERSION 2>/dev/null'
      bundle exec knife ssh -e -a fqdn roles:gstg-base-stor-gitaly sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure
      bundle exec knife ssh -e -a fqdn roles:gstg-base-stor-gitaly sudo gitlab-ctl restart gitaly
      bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo gitlab-rake gitlab:track_deployment
      bundle exec knife ssh -e -a fqdn roles:gstg-base-stor-gitaly OR roles:gstg-base-be-sidekiq OR roles:gstg-base-be-mailroom OR roles:gstg-base-fe-web OR roles:gstg-base-fe-web-pages OR roles:gstg-base-fe-api OR roles:gstg-base-fe-git OR roles:gstg-base-fe-registry OR roles:gstg-base-deploy-node sudo -E service chef-client start
      bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl start
      bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl deploy-page down
      bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo gitlab-rake db:migrate:status
      New version: 10.0.2-ee.0
      OUTPUT
    end

    subject { described_class.new(version: '10.0.2-ee.0', env: 'gstg', repo: 'gitlab/test', stop_sidekiq: false, skip_steps: ['pull_repo', 'load_balancing_check', 'update_version', 'deploy_package', 'migrations', 'deploy_mediator', 'post_migrations', 'post_deploy_mediator']) }

    it 'outputs the right command' do
      expect(subject).not_to receive(:abort)

      expect { subject.run }.to output(runner_output).to_stdout_from_any_process
    end
  end

  context 'run step pull_repo' do
    let(:runner_output) do
      <<~OUTPUT
      git checkout master
      git pull git@gitlab.com:gitlab-org/takeoff.git master
      OUTPUT
    end

    subject { described_class.new(version: '10.0.2-ee.0', env: 'gstg', repo: 'gitlab/test', stop_sidekiq: false, run_steps: ['pull_repo']) }

    it 'outputs the right command' do
      expect(subject).not_to receive(:abort)

      expect { subject.run }.to output(runner_output).to_stdout_from_any_process
    end
  end

  context 'run steps chef_stop, chef_start' do
    let(:runner_output) do
      <<~OUTPUT
      bundle exec knife ssh -e -a fqdn roles:gstg-base-stor-gitaly OR roles:gstg-base-be-sidekiq OR roles:gstg-base-be-mailroom OR roles:gstg-base-fe-web OR roles:gstg-base-fe-web-pages OR roles:gstg-base-fe-api OR roles:gstg-base-fe-git OR roles:gstg-base-fe-registry OR roles:gstg-base-deploy-node sudo service chef-client stop
      bundle exec knife ssh -e -a fqdn roles:gstg-base-stor-gitaly OR roles:gstg-base-be-sidekiq OR roles:gstg-base-be-mailroom OR roles:gstg-base-fe-web OR roles:gstg-base-fe-web-pages OR roles:gstg-base-fe-api OR roles:gstg-base-fe-git OR roles:gstg-base-fe-registry OR roles:gstg-base-deploy-node sudo -E service chef-client start
      OUTPUT
    end

    subject { described_class.new(version: '10.0.2-ee.0', env: 'gstg', repo: 'gitlab/test', stop_sidekiq: false, run_steps: ['chef_stop', 'chef_start']) }

    it 'outputs the right command' do
      expect(subject).not_to receive(:abort)

      expect { subject.run }.to output(runner_output).to_stdout_from_any_process
    end
  end
  
  context 'run an invalid step' do
    
    subject { described_class.new(env: 'gstg', run_steps: ['chfe_stop', 'chef_start']) }

    it 'raises an error' do
      expect { subject.run }.to raise_error('`chfe_stop` is not a valid step')
    end
  end

  context 'skip an invalid step' do
    
    subject { described_class.new(env: 'gstg', skip_steps: ['chfe_stop', 'chef_start']) }

    it 'raises an error' do
      expect { subject.run }.to raise_error('`chfe_stop` is not a valid step')
    end
  end
end

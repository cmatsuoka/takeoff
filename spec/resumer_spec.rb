# frozen_string_literal: true

require 'spec_helper'
require './lib/resumer'
require 'json'
require 'tmpdir'

describe Resumer do
  let(:resumer) { described_class.new(true) }

  before do
    enable_dry_run
  end

  describe 'without resuming' do
    it 'has the right date' do
      run_in_tmp do
        expect(resumer.start_time).to be_a(Time)
      end
    end

    it 'has the right current_step' do
      run_in_tmp do
        expect(resumer.current_step).to eq(0)
      end
    end

    it 'sets the current step' do
      run_in_tmp do
        expect { resumer.set_current_step(4) }.to change { resumer.current_step }.to(4)
      end
    end

    it 'adds a substep' do
      run_in_tmp do
        expect { resumer.add_substep('test-node') }.to change { resumer.has_substep?('test-node') }.from(false).to(true)
      end
    end

    it 'skips a step that was already done' do
      allow(resumer).to receive(:current_step).and_return(5)

      run_in_tmp do
        expect(resumer.skip?(4)).to be true
      end
    end

    it 'does not skip the last step in failure' do
      allow(resumer).to receive(:current_step).and_return(5)

      run_in_tmp do
        expect(resumer.skip?(5)).to be false
      end
    end

    it 'does not skip newer steps' do
      allow(resumer).to receive(:current_step).and_return(5)

      run_in_tmp do
        expect(resumer.skip?(7)).to be false
      end
    end
  end

  context 'resuming' do
    it 'removes the file on success' do
      run_in_tmp_resuming do |filename|
        expect { resumer.success }.to change { File.exist?(filename) }.from(true).to(false)
      end
    end

    it 'has the right date' do
      run_in_tmp_resuming do
        expect(resumer.start_time).to be_a(Time)
      end
    end

    it 'has the right current_step' do
      run_in_tmp_resuming do
        expect(resumer.current_step).to eq(3)
      end
    end

    it 'sets the current step' do
      run_in_tmp_resuming do
        expect { resumer.set_current_step(4) }.to change { resumer.current_step }.to(4)
      end
    end

    it 'has the old substep' do
      run_in_tmp_resuming do
        expect(resumer.has_substep?('test-node-old')).to be true
      end
    end

    it 'adds a substep' do
      run_in_tmp_resuming do
        expect { resumer.add_substep('test-node') }.to change { resumer.has_substep?('test-node') }.from(false).to(true)
      end
    end
  end

  def run_in_tmp
    Dir.mktmpdir do |dir|
      filename = "#{dir}/.resumer"
      stub_const('Resumer::STATE_FILE', filename)

      yield(filename)
    end
  end

  def run_in_tmp_resuming
    run_in_tmp do |filename|
      File.open(filename, 'w') do |f|
        f.write(
          {
            current_step: 3,
            substeps: ['test-node-old'],
            start_time: Time.parse("2000-06-21 12:30:55 UTC")
          }.to_json)
      end
      yield(filename)
    end
  end
end

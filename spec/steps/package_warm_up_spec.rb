# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/package_warm_up'

describe Steps::PackageWarmUp do
  before { enable_dry_run }

  subject { described_class.new(version: '10.0.2',
                                environment: 'gstg',
                                repo: 'gitlab/pre-release') }

  it 'outputs the right command' do
    command = <<~COMMAND
      bundle exec knife ssh -e -a fqdn roles:gstg-base-stor-gitaly OR roles:gstg-base-be-sidekiq OR roles:gstg-base-be-mailroom OR roles:gstg-base-fe-web OR roles:gstg-base-fe-web-pages OR roles:gstg-base-fe-api OR roles:gstg-base-fe-git OR roles:gstg-base-fe-registry OR roles:gstg-base-deploy-node sudo apt-get -qq update && sudo apt-get install -d -y -q --force-yes gitlab-ee=10.0.2
    COMMAND

    expect { subject.run }.to output(command).to_stdout_from_any_process
  end
end

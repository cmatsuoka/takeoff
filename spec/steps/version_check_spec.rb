# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/version_check'

describe Steps::VersionCheck do
  subject { described_class.new(Roles.new('gstg')) }

  context 'same version' do
    let(:version_output) do
      <<~OUTPUT
      web-01-sv-gstg.c.gitlab-staging-1.internal                Current Version: gitlab-ee 10.0.2-ee.0 web-01-sv-gstg.c.gitlab-staging-1.internal
      api-01-sv-gstg.c.gitlab-staging-1.internal                Current Version: gitlab-ee 10.0.2-ee.0 api-01-sv-gstg.c.gitlab-staging-1.internal
      sidekiq-pages-01-sv-gstg.c.gitlab-staging-1.internal      Current Version: gitlab-ee 10.0.2-ee.0 sidekiq-pages-01-sv-gstg.c.gitlab-staging-1.internal
      sidekiq-pullmirror-01-sv-gstg.c.gitlab-staging-1.internal Current Version: gitlab-ee 10.0.2-ee.0 sidekiq-pullmirror-01-sv-gstg.c.gitlab-staging-1.internal
      git-01-sv-gstg.c.gitlab-staging-1.internal                Current Version: gitlab-ee 10.0.2-ee.0 git-01-sv-gstg.c.gitlab-staging-1.internal
      file-01-stor-gstg.c.gitlab-staging-1.internal             Current Version: gitlab-ee 10.0.2-ee.0 file-01-stor-gstg.c.gitlab-staging-1.internal
      sidekiq-besteffort-01-sv-gstg.c.gitlab-staging-1.internal Current Version: gitlab-ee 10.0.2-ee.0 sidekiq-besteffort-01-sv-gstg.c.gitlab-staging-1.internal
      mailroom-01-sv-gstg.c.gitlab-staging-1.internal           Current Version: gitlab-ee 10.0.2-ee.0 mailroom-01-sv-gstg.c.gitlab-staging-1.internal
      registry-01-sv-gstg.c.gitlab-staging-1.internal           Current Version: gitlab-ee 10.0.2-ee.0 registry-01-sv-gstg.c.gitlab-staging-1.internal
      sidekiq-realtime-01-sv-gstg.c.gitlab-staging-1.internal   Current Version: gitlab-ee 10.0.2-ee.0 sidekiq-realtime-01-sv-gstg.c.gitlab-staging-1.internal
      sidekiq-besteffort-02-sv-gstg.c.gitlab-staging-1.internal Current Version: gitlab-ee 10.0.2-ee.0 sidekiq-besteffort-02-sv-gstg.c.gitlab-staging-1.internal
      file-02-stor-gstg.c.gitlab-staging-1.internal             Current Version: gitlab-ee 10.0.2-ee.0 file-02-stor-gstg.c.gitlab-staging-1.internal
      sidekiq-traces-01-sv-gstg.c.gitlab-staging-1.internal     Current Version: gitlab-ee 10.0.2-ee.0 sidekiq-traces-01-sv-gstg.c.gitlab-staging-1.internal
      sidekiq-asap-01-sv-gstg.c.gitlab-staging-1.internal       Current Version: gitlab-ee 10.0.2-ee.0 sidekiq-asap-01-sv-gstg.c.gitlab-staging-1.internal
      sidekiq-besteffort-03-sv-gstg.c.gitlab-staging-1.internal Current Version: gitlab-ee 10.0.2-ee.0 sidekiq-besteffort-03-sv-gstg.c.gitlab-staging-1.internal
      OUTPUT
    end

    before do
      enable_dry_run
      allow_any_instance_of(Steps::VersionCheck).to receive(:dpkg_gitlab_version).and_return(version_output)
    end

    it 'outputs the right version' do
      expect { subject.run! }.to output("New version: 10.0.2-ee.0\n").to_stdout_from_any_process
    end
  end

  context 'different version' do
    let(:version_output) do
      <<~OUTPUT
      host1.gitlab.com      Current Version: gitlab-ee 10.0.2-ee.0 host1.gitlab.com
      host2.gitlab.com      Current Version: gitlab-ee 10.0.1-ee.0 host2.gitlab.com
      host3.gitlab.com      Current Version: gitlab-ee 10.0.2-ee.0 host3.gitlab.com
      OUTPUT
    end

    before do
      enable_dry_run
      allow_any_instance_of(Steps::VersionCheck).to receive(:dpkg_gitlab_version).and_return(version_output)
    end

    it 'outputs the right version' do
      expected_output = <<~OUTPUT
        \e[1m\e[31mHosts running different versions:\e[0m\e[0m
        \e[1m10.0.2-ee.0: \e[0mhost1.gitlab.com host3.gitlab.com
        \e[1m10.0.1-ee.0: \e[0mhost2.gitlab.com
      OUTPUT

      expect { subject.run! }.to output(expected_output).to_stdout_from_any_process
    end
  end
end

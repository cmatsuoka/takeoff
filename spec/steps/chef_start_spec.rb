# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/chef_start'

describe Steps::ChefStart do
  before { enable_dry_run }

  context 'blessed node' do
    subject { described_class.new(Roles.new('gstg')) }

    it 'outputs the right command' do
      command = <<~COMMAND.strip.tr("\n", ' ')
        bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo -E chef-client
      COMMAND

      expect { subject.run }.to output(/#{command}/).to_stdout_from_any_process
    end
  end

  context 'other nodes' do
    let(:roles) { Roles.new('gstg') }
    subject { described_class.new(roles, roles_to_start: roles.regular_roles) }

    it 'outputs the right command' do
      command = <<~COMMAND.strip.tr("\n", ' ')
        bundle exec knife ssh -e -a fqdn roles:gstg-base-stor-gitaly OR
        roles:gstg-base-be-sidekiq OR roles:gstg-base-be-mailroom OR
        roles:gstg-base-fe-web OR roles:gstg-base-fe-web-pages OR
        roles:gstg-base-fe-api OR roles:gstg-base-fe-git OR
        roles:gstg-base-fe-registry
        sudo -E service chef-client start
      COMMAND

      expect { subject.run }.to output(/#{command}/).to_stdout_from_any_process
    end
  end
end

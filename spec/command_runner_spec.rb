# frozen_string_literal: true

require 'spec_helper'
require './lib/command_runner'
require './lib/lb/config'
require './lib/lb/command_wrapper'
require 'byebug'

describe CommandRunner do
  before do
    enable_dry_run

    allow_open3_to_return(true)
  end

  describe '#run_command_on_roles' do
    it 'runs commands with knife' do
      expect(Open3).to receive(:popen3).with("echo bundle exec knife ssh -e -a fqdn 'roles:gstg-base-fe-api' 'date'")

      described_class.run_command_on_roles('gstg-base-fe-api', 'date')
    end
  end

  describe '#run_command_concurrently' do
    it 'runs commands with knife' do
      allow_any_instance_of(Lb::Config).to receive(:lb_list).and_return('lb-01:lb-02')

      wrapper = Lb::CommandWrapper.new('gstg')

      expect(Open3).to receive(:popen3).with("echo bundle exec knife ssh -e -C 3 -a fqdn 'roles:gstg-base-fe-api' 'sudo /usr/local/bin/ha-ctl -l lb-01:lb-02 -c \"date && sleep 3\"'")

      described_class.run_command_concurrently('gstg-base-fe-api', 'date', command_wrapper: wrapper)
    end
  end
end

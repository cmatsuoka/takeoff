# frozen_string_literal: true

Dir['./lib/steps/*.rb'].each { |file| require file }

module Steps
  def self.[](step_name)
    const_get("Steps::#{step_name.split('_').map(&:capitalize).join}")
  end

  def self.number(step_class)
    class_name = step_class.name.split('::').last

    # Taken from Rails's `underscore` method
    name = class_name.gsub(/([A-Z]+)([A-Z][a-z])/, '\1_\2').gsub(/([a-z\d])([A-Z])/, '\1_\2').tr("-", "_").downcase

    Takeoff.steps.index { |step| step.keys.first == name }
  end

  def self.step_name(step)
    step.class.name.sub('Steps::', '')
  end
end

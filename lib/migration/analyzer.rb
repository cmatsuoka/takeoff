require 'tty-table'

module Migration
  class Analyzer
    include TimeUtil

    attr_reader :error

    def initialize(migration_output, type:)
      @output = migration_output
      @table = []
      @total = 0
      @type = type
      @error = nil

      parse_output
    end

    def total_time
      @total_time ||= time_elapsed(@total, include_ago: false)
    end

    def any?
      @total > 0
    end

    def as_table
      @built_table ||= build_table

      "\n#{TTY::Table.new(table_header, @built_table).render(:ascii)}"
    end

    private

    def table_header
      ["#{@type} ( > #{min_seconds} seconds)", 'Duration']
    end

    def parse_output
      @output.each_line.with_index do |line, index|
        if line.include?('An error has occurred, all later migrations canceled:')
          find_error(index)

          break
        end

        migration, seconds = line.scan(/(\w*)\: migrated \((.*)s\)/)&.first

        next unless seconds = seconds&.to_f

        @total += seconds

        if seconds > min_seconds
          @table << [migration, seconds]
        end
      end
    end

    def find_error(index)
      output_lines = @output.lines
      migration_line = output_lines.reverse_each.detect { |line| line.include?(': migrating') }
      migration_name = migration_line&.scan(/(\w*)\: migrating/)&.first&.first

      @error =  migration_name ? output_lines[index+2]&.sub(/\A[\d.]*/, migration_name).strip : migration_name
    end

    def build_table
      if any?
        @table = @table.sort_by(&:last).reverse
        @table.each { |row| row[1] = time_elapsed(row[1], include_ago: false) }

        @table << ['Total (all timings)', total_time]
      end

      @table << ['Error', @error.split(' ').first] if @error

      @table
    end

    def min_seconds
      Takeoff.config[:migration_threshold].to_i
    end
  end
end

# frozen_string_literal: true
require './config/takeoff'

module CommandRunner
  extend self

  KNIFE_COMMAND = "bundle exec knife ssh #{Takeoff.config[:knife_options]} #{Takeoff.fqdn_or_ipaddress}".freeze

  # Runs a command with a fancy title and progress spinner.
  #
  # Upon success no output is displayed, unless silencing of STDOUT has been
  # disabled; in which case the output is written to STDOUT.
  #
  # Upon an error both the output of STDOUT and STDERR of the command is displayed
  # (in their respective output streams).
  #
  # command - The command to execute.
  # title - The title to display, defaults to the command.
  # silence_stdout - When set to `false` the output in STDOUT will be displayed.
  #
  # Returns the output of STDOUT and STDERR of the executed process. Raises an
  # error upon the command not executing properly.
  def run_with_progress(command, title: nil, silence_stdout: true, spinners: nil, &block)
    runner = ProgressRunner.new(command, title: title, silence_stdout: silence_stdout, spinners: spinners, &block)
    runner.execute(verbose: Takeoff.config[:verbose], dry_run: Takeoff.config[:dry_run])
  end

  # Runs a command on all hosts with the given Chef role.
  #
  # roles - A list of roles to run the command on.
  # command - The command to execute.
  def run_command_on_roles(roles, command, *args, &block)
    search_query = Array(roles).reject { |role| role.strip.empty? }.map { |role| "roles:#{role}" }.join(' OR ')
    raise "Command '#{command}' got an empty list of roles, arguments are #{args}. aborting execution" if search_query.empty?
    run_with_progress(
      "#{KNIFE_COMMAND} '#{search_query}' '#{command}'",
      *args,
      &block
    )
  end

  def run_command_concurrently(roles, command, args, &block)
    search_query = Array(roles).reject { |role| role.strip.empty? }.map { |role| "roles:#{role}" }.join(' OR ')

    raise "Command '#{command}' got an empty list of roles, arguments are #{args}. aborting execution" if search_query.empty?

    wrapper = args.delete(:command_wrapper)

    if roles.is_a?(Array) && roles.any? { |role| wrapper.has_lb?(role) }
      wrapped_command = wrapper.run_command(command: command)

      run_with_progress(
        "#{knife_concurrent_command(roles)} '#{search_query}' '#{wrapped_command}'",
        args,
        &block
      )
    elsif roles.is_a?(String) && wrapper.has_lb?(roles)
      wrapped_command = wrapper.run_command(command: command, role: roles)

      run_with_progress(
        "#{knife_concurrent_command(roles)} '#{search_query}' '#{wrapped_command}'",
        args,
        &block
      )
    else
      run_with_progress(
        "#{KNIFE_COMMAND} '#{search_query}' '#{command}'",
        args,
        &block
      )
    end
  end

  def knife_concurrent_command(roles)
    concurrency_key = roles.is_a?(Array) ? nil : roles.split('-').last.to_sym
    concurrency = Takeoff.config[:knife_concurrency][concurrency_key]

    "bundle exec knife ssh #{Takeoff.config[:knife_options]} -C #{concurrency} -a #{Takeoff.config[:ip_address] ? 'ipaddress' : 'fqdn'}".freeze
  end
end

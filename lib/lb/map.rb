module Lb
  class Map
    PADDING = 25

    def initialize
      @hash = Hash.new {|h,k| h[k] = [] }
      @style_map = Dispel::StyleMap.new(50)
      @output = "Updated at: #{Time.now.utc} - (^P Set everything to ready) (^Q Quit)"
    end

    def << (server_with_status)
      @hash[server_with_status[0]] << server_with_status[1]
    end

    def show
      @hash.each_with_index do |(key, values), index|
        up = values.count { |v| v == 'UP' }
        row = "\n#{key} (#{up}/#{values.size})"
        row += "#{' ' * (PADDING - row.size)}#{values.join(' ')}"
        index += 1

        build_map(index, row)

        @output << row
      end

      [@output, @style_map]
    end

    private

    def build_map(index, row)
      @style_map.add(:reverse, index, 0..PADDING)

      row.enum_for(:scan, /(?=UP)/).map { Regexp.last_match.offset(0).first }.each do |scan_index|
        @style_map.add(["#ffffff", "#00aa00"], index, scan_index - 1..scan_index + 1)
      end

      row.enum_for(:scan, /(?=MAINT)/).map { Regexp.last_match.offset(0).first }.each do |scan_index|
        @style_map.add(["#ffffff", "#aa0000"], index, scan_index - 1..scan_index + 4)
      end

      row.enum_for(:scan, /(?=DRAIN)/).map { Regexp.last_match.offset(0).first }.each do |scan_index|
        @style_map.add(["#ffffff", "#aa5500"], index, scan_index - 1..scan_index + 4)
      end
    end
  end
end

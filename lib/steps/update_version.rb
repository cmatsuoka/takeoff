# frozen_string_literal: true

require './lib/steps/base_roles'
require './config/takeoff'
require 'json'

module Steps
  class UpdateVersion < BaseRoles
    def run
      download_role

      if Takeoff.config[:dry_run]
        puts 'Ignoring version update to ' + environment

        return
      end

      update_role

      FileUtils.rm(role_path)
    end

    private

    def download_role
      run_with_progress "bundle exec knife download #{role_path}",
                        title: 'Updating local version role'
    end

    def update_attributes
      @attributes['repo'] = repo
      @attributes['version'] = version
      @attributes['use_key'] = Takeoff.config[:use_package_server_key]

      File.open(role_path, 'w') do |handle|
        handle.write(JSON.pretty_generate(json, indent: '  '))
      end
    end

    def update_role
      if same_repo_and_version? && use_package_server_key?
        puts 'Version role already up to date'
      else
        update_attributes

        run_with_progress "bundle exec knife role from file #{role_path}",
                          title: 'Uploading version role changes to Chef'
      end
    end

    def use_package_server_key?
      attributes['use_key'] == Takeoff.config[:use_package_server_key]
    end

    def same_repo_and_version?
      attributes['repo'] == repo && attributes['version'] == version
    end

    def attributes
      @attributes ||= json['default_attributes']['omnibus-gitlab']['package']
    end

    def json
      @json ||= JSON.parse(File.read(role_path))
    end

    def environment
      @environment ||= @options[:environment] == 'production' ? 'gitlab' : @options[:environment]
    end

    def version
      @options[:version]
    end

    def repo
      @options[:repo]
    end

    def role_path
      @role_path ||= "roles/#{environment}-omnibus-version.json"
    end

    def roles_to_stop
      @roles_to_stop ||= roles.regular_and_blessed
    end
  end
end

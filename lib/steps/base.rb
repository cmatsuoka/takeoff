# frozen_string_literal: true
require './lib/post_checks'
require 'semantic_logger'

module Steps
  class Base
    include SemanticLogger::Loggable

    attr_reader :options
    private :options

    attr_reader :error

    def initialize(options = {})
      @options = options
      @error = nil
    end

    def run!
      pre_checks
      logger.debug('Start')

      begin
        logger.measure_info('Finished', log_exception: :full, on_exception_level: :error) { run }
      rescue SystemExit, Interrupt => e
        slack.deploy_interrupted(self)

        raise e
      rescue => e
        #puts e.backtrace # TODO Log this to a file
        @error = e.message || "#{e.class}: No error message"
      end

      post_checks
    end

    def restart
      @error = nil

      begin
        run
      rescue => e
        @error = e.message || "#{e.class}: No error message"
      end
    end

    def run_with_progress(*args)
      CommandRunner.run_with_progress(*args)
    end

    def pre_checks; end

    def post_checks
      options['post_checks']&.each do |post_check|
        PostChecks[post_check].new(@error, step: self).run
      end

      if @error
        slack&.deploy_error(self)
        abort(@error) if slack
      end
    end

    def slack
      options[:slack_webhook]
    end

    def abort(message)
      raise ScriptError, message
    end
  end
end

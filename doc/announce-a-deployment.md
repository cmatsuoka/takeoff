# Announce a deployment

## Staging deployment

Public announcements are not necessary when deploying to staging.
However, you should still announce that you're going to deploy to staging in the
`#production` channel.

## Production deployment

Production deployments should always be announced well ahead of the actual
deploy, using both Twitter and GitLab broadcast messages.

### General guidelines for downtime deployment

Downtime deployments should be announced with a message detailing the downtime
(include a link to the relevant issue). Consider announcing multiple days in
advance (on Twitter) if known that far in advance; then via Twitter at
`T - 24 hours`, again at `T - 12 hours`, and tweet + post the
[broadcast message](#broadcast-message) at `T - 4 hours`.

If it is not known this far in advance that a deployment will cause downtime and
the deployment must happen, then it should be announced at least one hour before
the deploy takes place.

### Twitter

Sending tweets can be done using the `!tweet` command in the `#production`
channel. For example:

```
!tweet "We will be deploying GitLab EE 8.10.13 at X:Y UTC, 15 mins of downtime is expected"
```

Regular production deployments should be announced at least 15 minutes before
the deploy takes place. For example:

```
!tweet "We are about to deploy GitLab EE 8.10.13 starting at X:Y UTC"
```

If you can not use the `!tweet` command you should ask for somebody else to
Tweet in the `#production` channel.

### Broadcast message

Setting a broadcast message can be done using the `!broadcast` command in the
`#production` channel. For example:

```
!broadcast --start A:B  --end X:Y  "We are currently deploying GitLab EE 9.3.3"
```

Alternatively, you can also set a broadcast message through the GitLab UI (this
requires an admin account).

#### Additional note for downtime deployment

For deployments that are expected to cause downtime, make sure to include a note.
For example:

```
!broadcast --start A:B  --end X:Y  "We are currently deploying GitLab EE 9.3.3.
For status updates during the downtime, please follow [@gitlabstatus](https://twitter.com/gitlabstatus)
```

and/or a link to the issue that describes the downtime. For example:

```
!broadcast --start A:B  --end X:Y  "We are currently [deploying GitLab EE 9.3.3](LINK_TO_THE_DEPLOYMENT_ISSUE).
For status updates during the downtime, please follow [@gitlabstatus](https://twitter.com/gitlabstatus)
```

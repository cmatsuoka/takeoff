# Troubleshooting deployments

Questions and answers about common issues that may occur while deploying using Takeoff

## Warmup

### What does the warmup actually do?

You can run the command  with `DRY_RUN=1` to see the actual knife commands. Useful to send this to #production in case of troubleshooting.

### Warmup takes longer than 5 minutes:

It should normally take about 2 minutes to download packages, if it takes longer it's probably because there is one node that is slow to download the packages. Knife can't tell us exactly which, but you can ask #Production to check by running `ps aux |grep dpkg` across all fleet, then checking the size of the package in `/var/cache/apt/archives/partial/`.

Or you can try this quick and dirty script that will list the contents of the `/var/cache/apt/archives/partial/` directory on each host, if there is any:

```
for host in `bundle exec bin/version | awk '{print $4}' | tr -d '\r'`; do ssh -q -o StrictHostKeyChecking=no $host 'echo "`hostname -f`:  "; sudo ls -lh /var/cache/apt/archives/partial/* 2>/dev/null'; done
```

### Warmup fails with: `E: dpkg was interrupted, you must manually run 'sudo dpkg --configure -a' to correct the problem.`

When it fails, you may need to grep for `E:` to see the actual errors. If you can't find it, please send the log to #production, and request to fix the node(s) affected by running `sudo dpkg --configure -a` on them.

### Warmup fails with: `E: Could not get lock /var/lib/dpkg/lock - open (11: Resource temporarily unavailable)`

Similar to the previous error, in this case we may need to run the installation again (you could try to run warmup again). If it keeps failing, please ask #production for assitance. 

## Deploy

### What does the deployment actually do?

You can run the command  with `DRY_RUN=1` to see the actual knife commands. Useful to send this to #production in case of troubleshooting.

### What does `--resume` do?

It resumes the last deployment, only deploying to those hosts that either had an error or didn't get the new version.

### Can I run or skip a single or multiple steps?

Yes, check `bin/takeoff-deploy --help`. You can list the steps with `--step-list` and skip them with `-s 1,2...`

### How do I check the progress?

You can use `bin/version` (with an optional `-r role (sidekiq, web, etc...)`) to see which nodes have the new version already and which are left.

### Failed to stop chef

This is pretty common. It should work on retry (we retry automatically), if it doesn't, it could be that some machines are in a inconsistent state and need to be checked by production.

### "Gitaly version has not changed"

This is fine and it just means that the hosts will remain in the old version as Gitaly didn't update, so once we output them at the end while checking the version, we should see the same hosts there still using the old version.

### Error installing in one of the nodes

For any error that stops the deployment, we should get #production to look at it. Once the issue is fixed, we can then resume the deployment using `--resume`. If for some reason, we don't resume the deployment, make sure the `chef-client` remains stopped until the correct version is in place.

### Why is Sidekiq taking so long?

Sidekiq can take anywhere between 40-60 minutes to deploy depending on how busy I/O in those nodes is. Please don't stop the deployment unless it's needed, as resuming again in this state will cause package lock issues.

### One of the migrations failed, what should I do?

You probably want to [roll back](#rolling-back), fix the issue and deploy an RC with just the relevant fixes. We shouldn't publish the packages.

### One of the post-deployment migrations failed, or we found a significant bug after post-deployment-migrations ran

At this point, it's best to get #development involved and [create a patch](https://dev.gitlab.org/gitlab/post-deployment-patches) to fix the issue ASAP. We shouldn't publish the packages and create an RC as soon as production is patched.

### Deployment seems stuck. What Should I do?

First, we need to know what `knife` command(s) is still running. You can do this locally using `ps aux | grep knife`. Once we know what is actually doing, it might be a good idea to go through the nodes from the role (for example sidekiq) and check the processes running in them (Please ask in #production for this step).

## Rolling back

We can always rollback as long as there were no post-deployment migrations or they did not run. It should be the usual deployment command with the previous version (no need to rollback migrations)

```mermaid
graph TD;
    A(Deploy Node)-->B(Migrations);
    B-->C(Other nodes);
    C-->D(Post-Deployment migrations);
    style A fill:#99e699
    style B fill:#99e699
    style C fill:#99e699
    style D fill:#ff8080
```

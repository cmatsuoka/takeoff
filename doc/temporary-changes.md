# Temporary Changes

Sometimes there's a need to quickly deploy (small) patches to GitLab.com without
first going through the process of setting up merge requests, updating GitLab
EE, building Omnibus packages, etc. An example is the need to test small
performance changes or performing emergency bug fixes.

## Preparations

The first step in this process should be asking yourself the question "Is this
really needed?". Making changes on a production environment is quite the risky
undertaking and may result in downtime.

If this process is indeed needed the next step should be to note down the GitLab
EE commit that's currently deployed to GitLab.com. To do so, go to
<https://gitlab.com/help> and note down the commit SHA1 as found in the title.
For this document we'll use SHA1 "791ad8d".

Also make sure you have the appropriate Chef permissions to run the "knife"
command.

## Setup

1. Make sure your local clone of GitLab EE is up to date and that the currently
   active branch is "master".
2. Run `git fetch` to make sure all tags and whatnot are also up to date.
3. Run `git checkout SHA` replacing `SHA` with the SHA1 noted above. For
   example `git checkout 791ad8d`.
4. Run `git checkout -b BRANCH` replacing `BRANCH` with the name of the branch
   you'd like to use for your temporary changes. This newly created branch will
   be based on the commit checked out in the previous step.
5. Make your changes and push them to GitLab.com
6. Note down the full URLs of all the files you plan to change as of the commit
   noted down earlier. These URLs will be later used to restore files to their
   original state. You can do this by navigating to the file using GitLab.com's
   file browser, clicking the "Permalink" button followed by clicking the "Raw"
   button.
7. Note down the full URLs of all the files changed on your branch, these URLs
   will be used to deploy the changes.

Once all done make sure to notify other operations engineers in the
`#production` Slack channel.

## Deployment

For all the URLs of the files to deploy, run the following command:

    knife ssh -aipaddress 'role:gitlab-cluster-worker' \
        'sudo curl --silent FILE-URL -o /opt/gitlab/embedded/service/gitlab-rails/path/to/file/relative/to/app && sudo gitlab-ctl hup unicorn'

If you want to exclude a certain worker use the following command instead:

    knife ssh -aipaddress 'role:gitlab-cluster-worker AND NOT name:workerX.cluster.gitlab.com' \
        'sudo curl --silent FILE-URL -o /opt/gitlab/embedded/service/gitlab-rails/path/to/file/relative/to/app && sudo gitlab-ctl hup unicorn'

These commands will download the files and reload all Unicorns. If you're
deploying multiple files make sure to only run `sudo gitlab-ctl hup unicorn`
after _all_ files have been deployed.

## Reverting

To revert the changes simply re-run the above commands but using the URLs
pointing to the original files.

Once changes have been reverted you should again post a notification in the
`#production` channel.
